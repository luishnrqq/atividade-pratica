O que é Git?
O que é a staging area?
O que é o working directory?
O que é um commit?
O que é uma branch?
O que é o head no Git?
O que é um merge?
Explique os 4 estados de um arquivo no Git.
Explique o comando git init.
Explique o comando git add.
Explique o comando git status.
Explique o comando git commit.
Explique o comando git log.
Explique o comando git checkout -b.
Explique o comando git reset e suas três opções.
Explique o comando git revert.
Explique o comando git clone.
Explique o comando git push
Explique o comando git pull.
Como ignorar o versionamento de arquivos no Git?
No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.


Respostas:

1. Git é um sistema de controle de versão distribuído usado para gerenciar e rastrear as alterações em arquivos de um projeto, ele permite varios desenvolvedores trabalharem em um mesmo projeto sem conflitos além de fornecer recursos para ver o histórico de alterações e reverter versões anteriores.

2. A staging area é uma área intermediária em que os arquivos modificados são preparados para serem incluídos no próximo commit







Q3. O código vai separar a entrada inserida pelo usuário através do slice e salva-los em subarrays, em seguida vai realizar a soma desses dois sub arrays

Q4. A mensagem de commit deve descrever brevemente as alterações feitas no arquivo, para que outros colaboradores possam entender facilmente o propósito do commit

Q5. A sintaxe das funções é diferente, enquanto a segunda usa sintaxe de "arrow function" . A primeira é função é chamada no momento em que é executada, ja no segundo codigo, a função soma é chamada posteriormente.

Q6. João na hora de fazer o console.log para mostrar a mensagem de erro fez a referência da variável errada, ao invés de escrever "args" ele usou "arg"

Q7. Para criar uma branch e automaticamente mudar pra ela pode-se utilizar o comando git checkout -b "new-branch"

Q8. Para fazer um merge, assim que realizar qualquer alteração em sua nova branch, você ira pra pagina do git lab e clicar em "create new branch", adcionar a branch que será o "destino" da alteração e fazer uma breve descrição das alterações